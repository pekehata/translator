/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator;

import java.util.HashMap;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.config.PersistenceUnitProperties;

/**
 * This class will manage Persistence Unit.
 * Singleton model is employed so that we can share the connection effectively.
 * You can get Entity Managet Factory or Entity Manager after getting class
 * through getInstance() method.
 */
public class ConnectItemPU {
    private static final ConnectItemPU instance = new ConnectItemPU();
    private EntityManagerFactory emf;
    private EntityManager em;
    private long lastConnectedTime = System.currentTimeMillis();
    private static String dbPath = "";

    /**
     * Leave this constructor private, otherwise you cannot benefit
     * the singleton model's profit.
     */
    private ConnectItemPU(){}
    
    /**
     * This method will be used internally to setup connection.
     * JDBC URL should be specified in this class,
     * and then get Entity Manage Factory and Entity Manager.
     * @return returns true if successfully created connection
     */
    private Boolean createNewConnection(){
        //Nothing to do if dbPath is empty
        if(dbPath.isEmpty()) return Boolean.FALSE;
        //Just make sure to em and emf is closed before creating new connection
        if( ! Objects.isNull(em) && em.isOpen()) em.close();
        if( ! Objects.isNull(emf) && emf.isOpen()) emf.close();
        
        //Set JDBC URL
        String jdbcURL = "jdbc:ucanaccess://" + dbPath + ";inactivityTimeout=0;showSchema=true;";
        HashMap<String, String> map = new HashMap();
        map.put(PersistenceUnitProperties.JDBC_URL, jdbcURL);

        //Set emf and em value
        emf = Persistence.createEntityManagerFactory("itemPU", map);
        try {
            em = emf.createEntityManager();
        } catch (PersistenceException pe1) {
            Logger.getLogger(ConnectItemPU.class.getName()).log(Level.SEVERE, null, pe1);
            MessageBox.show(Translator.stage,
                    "Failed to connect Database.", "Warning", MessageBox.OK);
            emf.close();
            return Boolean.FALSE;
        }
        System.out.println("Connected to " + jdbcURL);
        return Boolean.TRUE;
    }
    
    /**
     * Return this class's instance which is singleton.
     * @return return the instance of this class.
     */
    public static ConnectItemPU getInstance(){
        return instance;
    }
    
    /**
     * Return the Entity Manager Factory defined in this instance.
     * @return 
     */
    private EntityManagerFactory getEMF(){
        if( ! emf.isOpen()){
            createNewConnection();
        }
        return emf;
    }

    /**
     * Return the Entity Manager Factory defined in this instance.
     * @return 
     * @throws IncorrectDatabaseFilePathException if cannot get available Entity Manager
     */
    public EntityManager getEM() throws IncorrectDatabaseFilePathException {
        //create new connection if em or emf is null/un-opened yet or 5 mins has passed since last connection
        if( Objects.isNull(emf) || Objects.isNull(em) ||
                ! em.isOpen() || ! emf.isOpen() ||
                System.currentTimeMillis() - lastConnectedTime > 300000){
            createNewConnection();
        }
        lastConnectedTime = System.currentTimeMillis();
        if(Objects.isNull(em))
            throw new IncorrectDatabaseFilePathException(PersistenceUnitProperties.JDBC_URL);
        return em;
    }
    
    /**
     * Set absolute path to the database.
     * @param dbpath specify absolute path to the database
     */
    public void setPathToDatabase(String dbpath) {
        ConnectItemPU.dbPath = dbpath;
        createNewConnection();
    }
}

/**
 * Just throw this exception in case the specified JDBC URL is wrong.
 */
class IncorrectDatabaseFilePathException extends Exception {
    public IncorrectDatabaseFilePathException(String jdbcURL) {
        super("The jdbc URL you tried to connect might be wrong [" + jdbcURL + "]");
    }
}