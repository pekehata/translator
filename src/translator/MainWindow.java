/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import jfx.messagebox.MessageBox;
import static translator.Parameter.*;

/**
 * This is controller class for <code>MainWindow.fxml</code>.
 * MainWindow should be the picture frame of nodes
 * which is implemented for specific feature.
 */
public class MainWindow implements Initializable {
    @FXML
    private VBox baseVBoxFX;
    @FXML
    private TextField dbpathFX;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Make the nodes fit to stage
        Double initialDBPathWidthGap = baseVBoxFX.getPrefWidth() - dbpathFX.getPrefWidth();
        baseVBoxFX.widthProperty().addListener(listener -> {
            dbpathFX.setPrefWidth(baseVBoxFX.getWidth() - initialDBPathWidthGap);
        });
        
        /**
         * Establish first connection to the database here,
         * because if we don't do it in initialization process here,
         * it will take time when the first query to the database is thrown.
        **/
        connectDatabase();
    }
    
    /**
     * Connect to the database, mainly intended for the initial connection
     * after the app is launched.
     */
    @FXML
    private void connectDatabase() {
        //declare the path for config file
        File classpath = new File(System.getProperty("java.class.path"));
        String configFilePath = classpath.getParentFile().toPath() + "/" + CONFIGFILENAME;

        //if dbpathFX is not empty, load path from it, connect db, write history and exit
        if( ! Objects.isNull(dbpathFX.getText()) && ! dbpathFX.getText().isEmpty())
            readFromDBPath(configFilePath);
        else
            readFromConfigfile(configFilePath);
    }
    
    /**
     * Try to connect DB path specified in TextField <code>dbpathFX</code>.
     * @param configFilePath absolute path to the config file used to write DB path.
     */
    private void readFromDBPath(String configFilePath) {
        File dbpath = new File(dbpathFX.getText());
        if( ! dbpath.exists()) {
            MessageBox.show(Translator.stage, "Please specify correct Database path.", "Warning", MessageBox.OK);
            return;
        }
        //Connect Database
        try {
            //Create Connection
            ConnectItemPU.getInstance().setPathToDatabase(dbpathFX.getText());
            ConnectItemPU.getInstance().getEM();

            //Write db file path to the config file
            StringBuilder sb = new StringBuilder("");
            FileReader fr = new FileReader(configFilePath);
            BufferedReader br = new BufferedReader(fr);
            String str;
            while ((str = br.readLine()) != null ) {
                if(str.startsWith(DBPATHPREFIX))
                    sb.append(DBPATHPREFIX)
                            .append(dbpathFX.getText())
                            .append("\n");
                else
                    sb.append(str)
                            .append("\n");
            }
            FileWriter fw = new FileWriter(configFilePath);
            fw.write(sb.toString());
            fw.close();
            br.close();
            MessageBox.show(Translator.stage, "Connected to the database below:\n" + dbpathFX.getText()
                    , "Information", MessageBox.OK);
        } catch (IncorrectDatabaseFilePathException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            MessageBox.show(Translator.stage, "Incorrect jdbc URL.", "Warning", MessageBox.OK);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            MessageBox.show(Translator.stage,
                    "This should never happen!\n"
                    + "Please check the write permission of the information file."
                    , "Error", MessageBox.OK);
        }
    }
    /**
     * Try to connect DB path specified in config file.
     * @param configFilePath absolute path to the config file to be read
     */
    private void readFromConfigfile(String configFilePath) {    
        //read path from file if nothing has been specified in dbpathFX
        FileReader fr;
        BufferedReader br;
        String line = "";
        try {
            fr = new FileReader(configFilePath);
            br = new BufferedReader(fr);
            //Search for dbpath value from the file
            while ((line = br.readLine()) != null) {
                if(line.startsWith(DBPATHPREFIX))
                    break;
            }
            br.close();
            fr.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            //File doesn't exist, so try to create new file
            try {
                File f = new File(configFilePath);
                f.createNewFile();
                f.setWritable(true);
            } catch (IOException ex1) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex1);
                MessageBox.show(Translator.stage,
                        "Cannot create information file.\n"
                        + "Please manually create blank file \"" + CONFIGFILENAME + "\" in the same directory of .jar file"
                        , "Error", MessageBox.OK);
                return;
            }
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if( Objects.isNull(line) || ! line.startsWith(DBPATHPREFIX)) {
            //Make a new line in the file with only DBPATHPREFIX
            try (FileWriter fw = new FileWriter(configFilePath)) {
                fw.write(DBPATHPREFIX + "\n");
                MessageBox.show(Translator.stage, "Couldn't read Path to the last Database you connected.\n"
                        + "Please specify correct path in the textbox on top."
                        , "Warning", MessageBox.OK);
            } catch (IOException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                MessageBox.show(Translator.stage,
                        "This should never happen!\n"
                        + "There must be a bug in source code..."
                        , "Error", MessageBox.OK);
            }
            return;
        }
        
        File db = new File(line.replaceFirst(DBPATHPREFIX, ""));
        if( ! db.exists()) {
            MessageBox.show(Translator.stage, "Couldn't read Path to the last Database you connected.\n"
                    + "Please specify correct path in the textbox on top."
                    , "Warning", MessageBox.OK);
            return;
        }
        
        //Set db path and try to connect
        dbpathFX.setText(db.toPath().toString());
        ConnectItemPU.getInstance().setPathToDatabase(db.toPath().toString());
        try {
            ConnectItemPU.getInstance().getEM();
        } catch (IncorrectDatabaseFilePathException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            MessageBox.show(Translator.stage,
                    "The database you connected most recently seems to be no longer available.",
                    "Warning", MessageBox.OK);
        }
    }
    
    /**
     * Show popup window to choose the database file
     * and set value to <code>dbpathFX</code>.
     */
    @FXML
    private void chooseFile() {
        FileChooser fc = new FileChooser();
        File choosedFile = fc.showOpenDialog(Translator.stage);
        if( ! Objects.isNull(choosedFile))
            dbpathFX.setText(choosedFile.getAbsolutePath());
    }
}