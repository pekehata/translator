/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator;

import translator.util.TabKeyEventHandler;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import jfx.messagebox.MessageBox;
import org.controlsfx.dialog.ExceptionDialog;
import static translator.Parameter.SUBSCRIPTIONKEY;

/**
 * This is controller class for <code>RegisterTab.fxml</code>.
 */
public class RegisterTab  implements Initializable {
    @FXML
    private TextField barcodeFX;
    @FXML
    private TextField itemnameFX;
    @FXML
    private TextField sizeFX;
    @FXML
    private TextArea ingredientsFX;
    @FXML
    private TextArea usesFX;
    @FXML
    private TextField cpriceFX;
    @FXML
    private TextArea ingredientsTranslatedFX;
    @FXML
    private TextArea usesTranslatedFX;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //In TextArea, tab key should not work as not "to insert spaces" but "to move focus to next object"
        ingredientsFX.addEventFilter(KeyEvent.KEY_PRESSED, new TabKeyEventHandler());
        ingredientsTranslatedFX.addEventFilter(KeyEvent.KEY_PRESSED, new TabKeyEventHandler());
        usesTranslatedFX.addEventFilter(KeyEvent.KEY_PRESSED, new TabKeyEventHandler());
        usesFX.addEventFilter(KeyEvent.KEY_PRESSED, new TabKeyEventHandler());
    }
    
    /**
     * Save the item specified in TextFields/TextAreas.
     * Data type of some fields may need to be examined.
     */
    @FXML
    private void saveData() {
        Item item = new Item();
        item.setItemname(itemnameFX.getText());
        item.setSize(sizeFX.getText());
        item.setIngredients(ingredientsTranslatedFX.getText());
        item.setUses(usesTranslatedFX.getText());
        item.setCprice(cpriceFX.getText());

        //Check the data type on barcode and cprice and then setText
        try {
            Integer i = new Integer(barcodeFX.getText());
            item.setBarcode(i.toString());
        } catch (NumberFormatException ex) {
            Logger.getLogger(RegisterTab.class.getName()).log(Level.SEVERE, null, ex);
            MessageBox.show(Translator.stage,
                    "You tried to put non-numeric letters or invalid value in the \"Barcode\" field.\n"
                            + "Please change the value and try \"Save\" again.",
                    "Error", MessageBox.OK);
            return;
        }
        try {
            Double d = new Double(cpriceFX.getText());
            item.setCprice(d.toString());
        } catch (NumberFormatException ex) {
            Logger.getLogger(RegisterTab.class.getName()).log(Level.SEVERE, null, ex);
            MessageBox.show(Translator.stage,
                    "You tried to put non-numeric letters or invalid value in the \"Price\" field.\n"
                            + "Please change the value and try \"Save\" again.",
                    "Error", MessageBox.OK);
            return;
        }
        
        //Store the item to the database
        EntityManager em;
        try {
            em = ConnectItemPU.getInstance().getEM();
        } catch (IncorrectDatabaseFilePathException ex) {
            MessageBox.show(Translator.stage, "Please specify the database file first.", "Warning", MessageBox.OK);
            return;
        }
        EntityTransaction tx = em.getTransaction();
        try {
            em.clear();
            tx.begin();
            em.persist(item);
            em.flush();
            tx.commit();
            em.clear();
            long start = System.currentTimeMillis();
            long end = System.currentTimeMillis();
            System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[2].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
            MessageBox.show(Translator.stage, "Saved data successfully.", "Information", MessageBox.OK);
        } catch (Exception ex) {
            tx.rollback();
            em.clear();
            ExceptionDialog ed = new ExceptionDialog(ex);
            ed.show();
        }
    }
    
    /**
     * Translating the data through Microsoft Translate API.
     * <p>### Caution ### Subscription key is hardcoded in source code of <code>Parameter.java</code>.
     * <p>For details, please refer to:
     * <p>https://azure.microsoft.com/ja-jp/services/cognitive-services/translator-text-api/
     */
    @FXML
    private void translateData() {
        try {
            //Need to get token before execute
            Translate.getToken(SUBSCRIPTIONKEY);
            Translate.setSubscriptionKey(SUBSCRIPTIONKEY);
            
            //Execute API query and set translated texts
            String ingredientsTranslated
                    = Translate.execute(ingredientsFX.getText(), Language.ENGLISH, Language.FRENCH);
            String usesTranslated
                    = Translate.execute(usesFX.getText(), Language.ENGLISH, Language.FRENCH);
            ingredientsTranslatedFX.setText(ingredientsTranslated);
            usesTranslatedFX.setText(usesTranslated);
        } catch (Exception ex) {
            ExceptionDialog ed = new ExceptionDialog(ex);
            ed.show();
        }
    }
    
    /**
     * Clear TextFields/TextAreas for new Item.
     */
    @FXML
    private void clearNewDataField() {
        barcodeFX.setText("");
        itemnameFX.setText("");
        sizeFX.setText("");
        ingredientsFX.setText("");
        usesFX.setText("");
        cpriceFX.setText("");
        ingredientsTranslatedFX.setText("");
        usesTranslatedFX.setText("");
    }
    
}
