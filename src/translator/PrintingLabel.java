/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator;

import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * This is controller class for <code>PrintingLabel.fxml</code>.
 */
public class PrintingLabel implements Initializable {

    @FXML
    private GridPane labelsGridPaneFX;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {}
    
    /**
     * Load each item's data into label sheet format.
     * @param itemList 
     */
    public void loadDataIntoLabels(List<Item> itemList) {
        int i = 0;
        for(Item item : itemList) {
            i++;
            //null item is blank label to skip used label
            if(Objects.isNull(item)) continue;
            
            //lookup labels by css id and set text
            Label companyinfoLabel = (Label) labelsGridPaneFX.lookup("#company_name_and_address" + i);
            Label itemnameLabel = (Label) labelsGridPaneFX.lookup("#item_name" + i);
            Label ingredientsLabel = (Label) labelsGridPaneFX.lookup("#ingredients" + i);
            Label useLabel = (Label) labelsGridPaneFX.lookup("#uses" + i);
            companyinfoLabel.setText(Parameter.COMPANYNAME + " / " + Parameter.COMPANYADDRESS);
            itemnameLabel.setText(item.getItemname());
            ingredientsLabel.setText(item.getIngredients());
            useLabel.setText(item.getUses());
        }
    }
}
