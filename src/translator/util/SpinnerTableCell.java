/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator.util;

import java.util.Objects;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableCell;
import translator.Item;

/**
 *
 * @author masanori
 */
public class SpinnerTableCell extends TableCell<Item, Integer> {
    private final Integer MINVALUE = 1;
    private final Spinner<Integer> spinner = new Spinner<Integer>(MINVALUE, Integer.MAX_VALUE, 1);

    public SpinnerTableCell() {
        spinner.setEditable(true);
        spinner.focusedProperty().addListener(new CommitSpinnerValue(spinner));
        spinner.valueProperty().addListener((listener) -> {
            this.updateItem(spinner.getValue(), false);
        });
        setGraphic(spinner);
    }

    @Override
    public void updateItem(Integer item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || Objects.isNull(this.getTableRow().getItem())) {
            spinner.getValueFactory().setValue(MINVALUE);
            spinner.setVisible(false);
            return;
        }
        spinner.setVisible(true);
        Item bindedItem = (Item) this.getTableRow().getItem();
        bindedItem.setPrintcounter(item);
    }
}
