/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import translator.Item;

/**
 *
 * @author masanori
 */
public class RoundedValueCallBack implements Callback<TableColumn.CellDataFeatures<Item, String>, ObservableValue<String>> {
    
    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<Item, String> param) {
        Item item = param.getValue();
        Double d;
        try {
            d = new Double(item.getCprice());
        } catch (NumberFormatException nfex) {
            System.out.println("Cannot format String to double in this item : " + item);
            return new ReadOnlyObjectWrapper<String>(item.getCprice());
        }
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.HALF_UP);
        return new ReadOnlyObjectWrapper<String>(df.format(d));
    }
    
}
