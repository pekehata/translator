/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator.util;

import com.sun.javafx.scene.control.behavior.TextAreaBehavior;
import com.sun.javafx.scene.control.skin.TextAreaSkin;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author masanori
 */
public class TabKeyEventHandler implements EventHandler<KeyEvent> {
    @Override
    public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.TAB) {
            TextArea target = (TextArea) event.getTarget();
            TextAreaSkin skin = (TextAreaSkin) target.getSkin();
            if (skin.getBehavior() instanceof TextAreaBehavior) {
                TextAreaBehavior behavior = (TextAreaBehavior) skin.getBehavior();
                if (event.isControlDown()) {
                    behavior.callAction("InsertTab");
                } else if (event.isShiftDown()) {
                    behavior.callAction("TraversePrevious");
                } else {
                    behavior.callAction("TraverseNext");
                }
                event.consume();
            }
        }
    }
    
}
