/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator.util;

import java.util.Objects;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;

/**
 *
 * @author masanori
 */
public class CommitSpinnerValue implements ChangeListener<Boolean> {
    
    Node node;

    public CommitSpinnerValue(Node node) {
        this.node = node;
    }

    @Override
    public void changed(ObservableValue observable, Boolean oldValue, Boolean newValue) {
        if (node instanceof TextField) {
            TextField tf = (TextField) node;
            if (tf.getText().length() == 0) {
                return;
            }
            if (!newValue) {
                tf.setText(tf.getText());
                if (!Objects.isNull(tf.getParent()) && tf.getParent() instanceof ComboBox<?> && tf.getText().length() != 0) {
                    ComboBox<?> cb = (ComboBox<?>) tf.getParent();
                    cb.getEditor().setText(Integer.decode(tf.getText()).toString());
                } else if (tf.getText().length() == 0) {
                    tf.setText("0");
                }
            }
            tf.commitValue();
        } else if (node instanceof Spinner) {
            Spinner<Integer> spinner = (Spinner) node;
            spinner.getEditor().setText(spinner.getEditor().getText());
            if (!Objects.isNull(spinner.getEditor().getText()) && !spinner.getEditor().getText().isEmpty()) {
                spinner.getValueFactory().valueProperty().setValue(Integer.decode(spinner.getEditor().getText()));
            } else {
                spinner.getValueFactory().valueProperty().setValue(0);
            }
        }
    }
    
}
