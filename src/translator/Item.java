/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator;

import java.io.Serializable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 * Entity class for item table.
 * The table contains each item's information such as barcode, item, etc..
 */
@Data
@Entity
@Table(name = "French")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i"),
    @NamedQuery(name = "Item.findByItemname", query = "SELECT i FROM Item i WHERE i.itemname = :itemname"),
    @NamedQuery(name = "Item.findByBarcode", query = "SELECT i FROM Item i WHERE i.barcode = :barcode"),
    @NamedQuery(name = "Item.findBySize", query = "SELECT i FROM Item i WHERE i.size = :size"),
    @NamedQuery(name = "Item.findByIngredients", query = "SELECT i FROM Item i WHERE i.ingredients = :ingredients"),
    @NamedQuery(name = "Item.findByUses", query = "SELECT i FROM Item i WHERE i.uses = :uses"),
    @NamedQuery(name = "Item.findByCprice", query = "SELECT i FROM Item i WHERE i.cprice = :cprice"),
    @NamedQuery(name = "Item.findByEprice", query = "SELECT i FROM Item i WHERE i.eprice = :eprice"),
    @NamedQuery(name = "Item.findByAllColumns", query = "SELECT i FROM Item i WHERE "
            + "i.itemname LIKE :itemname AND "
            + "i.barcode LIKE :barcode AND "
            + "i.size LIKE :size AND "
            + "i.ingredients LIKE :ingredients AND "
            + "i.uses LIKE :uses AND "
            + "i.cprice LIKE :cprice "
            + "order by i.itemname")})
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "barcode")
    private String barcode;
    @Id
    @Column(name = "itemname")
    private String itemname;
    @Id
    @Column(name = "size")
    private String size;
    @Id
    @Column(name = "ingredients")
    private String ingredients;
    @Id
    @Column(name = "uses")
    private String uses;
    @Id
    @Column(name = "cprice")
    private String cprice;

    //eprice is not used in this application but just leave them defined for the future use.
    @Id
    @Column(name = "eprice")
    private String eprice;
    
    /**
     * Field "printcounter" will be used for printing job only,
     * so value shouldn't be stored in Database.
     */
    @Transient
    private final IntegerProperty printcounter = new SimpleIntegerProperty();
    public Integer getPrintcounter() {
        return this.printcounter.getValue();
    }
    public void setPrintcounter(Integer printcounter) {
        this.printcounter.set(printcounter);
    }
    public IntegerProperty printcounterProperty() {
        return this.printcounter;
    }
}
