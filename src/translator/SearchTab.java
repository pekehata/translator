/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator;

import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import jfx.messagebox.MessageBox;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import translator.util.*;

/**
 * This is controller class for <code>SearchTab.fxml</code>.
 */
public class SearchTab implements Initializable {
    @FXML
    private TextField barcodeSearchFX;
    @FXML
    private TextField itemnameSearchFX;
    @FXML
    private TextField sizeSearchFX;
    @FXML
    private TextArea ingredientsSearchFX;
    @FXML
    private TextArea usesSearchFX;
    @FXML
    private TextField cpriceSearchFX;
    
    @FXML
    private TableView<Item> searchResultTableFX;
    @FXML
    private TableColumn<Item, String> cpriceSearchResultFX;

    @FXML
    private TableView<Item> printedItemTableFX;
    @FXML
    private TableColumn<Item, Integer> printcounterColumnFX;
    @FXML
    private TableColumn<Item, String> cpriceToBePrintedColumnFX;
    
    @FXML
    private Spinner<Integer> skipLabelCounterFX;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Set place holder of tables (it is shown when table is empty)
        searchResultTableFX.setPlaceholder(new Text("Search Result"));
        printedItemTableFX.setPlaceholder(new Text("Items which will be printed"));
        
        //Single row, not multiple rows, should be selected at once on the table(s) below:
        searchResultTableFX.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        //Multiple rows, not single row, should be selected at once on the table(s) below:
        printedItemTableFX.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        //Set action for when search result table is clicked or key is pressed 
        searchResultTableFX.setOnMouseClicked(me -> {
            me.consume();
            //Just react to double-click
            if ( me.getClickCount() % 2 == 1) return;
            
            //Add item to printedItemTableFX if there isn't same record already
            Item selectedItem = searchResultTableFX.getSelectionModel().getSelectedItem();
            selectedItem.setPrintcounter(1);
            if( ! printedItemTableFX.getItems().contains(selectedItem))
                printedItemTableFX.getItems().add(selectedItem);
        });
        searchResultTableFX.setOnKeyPressed(ke -> {
            ke.consume();
            switch (ke.getCode()) {
                case ENTER:
                    //Add item to printedItemTableFX if there isn't same record already
                    Item selectedItem = searchResultTableFX.getSelectionModel().getSelectedItem();
                    selectedItem.setPrintcounter(1);
                    if( ! printedItemTableFX.getItems().contains(selectedItem))
                        printedItemTableFX.getItems().add(selectedItem);
                    break;
                case UP:
                    searchResultTableFX.getSelectionModel().selectPrevious();
                    break;
                case DOWN:
                    searchResultTableFX.getSelectionModel().selectNext();
                    break;
                default:
                    break;
            }
        });
        //In TextArea, tab key should not work as not "to insert spaces" but "to move focus to next object"
        ingredientsSearchFX.addEventFilter(KeyEvent.KEY_PRESSED, new TabKeyEventHandler());
        usesSearchFX.addEventFilter(KeyEvent.KEY_PRESSED, new TabKeyEventHandler());
        
        //Round half-up the values in cprice columns
        cpriceSearchResultFX.setCellValueFactory(new RoundedValueCallBack());
        cpriceToBePrintedColumnFX.setCellValueFactory(new RoundedValueCallBack());
        
        //Add spinner to the column in TableView
        printcounterColumnFX.setCellFactory(value -> new SpinnerTableCell());
        
        //Setup spinner
        skipLabelCounterFX.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 29, 0));
        skipLabelCounterFX.focusedProperty().addListener(new CommitSpinnerValue(skipLabelCounterFX));
    }
    
    /**
     * Clear the text input in the fields for search
     */
    @FXML
    private void clearSearchField() {
        barcodeSearchFX.setText("");
        itemnameSearchFX.setText("");
        sizeSearchFX.setText("");
        cpriceSearchFX.setText("");
        ingredientsSearchFX.setText("");
        usesSearchFX.setText("");
    }
    
    /**
     * Throw a search query with parameters specified in TextFields.
     */
    @FXML
    private void searchData() {
        //Try to get Entity Manager
        EntityManager em;
        try {
            em = ConnectItemPU.getInstance().getEM();
        } catch (IncorrectDatabaseFilePathException ex) {
            MessageBox.show(Translator.stage, "Please specify the database file first.", "Warning", MessageBox.OK);
            return;
        }
        
        //Prepare for the query
        TypedQuery tquery = em.createNamedQuery("Item.findByAllColumns", Item.class);
        tquery.setParameter("itemname", "%" + itemnameSearchFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
        tquery.setParameter("barcode", "%" + barcodeSearchFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
        tquery.setParameter("size", "%" + sizeSearchFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
        tquery.setParameter("ingredients", "%" + ingredientsSearchFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
        tquery.setParameter("uses", "%" + usesSearchFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");
        tquery.setParameter("cprice", "%" + cpriceSearchFX.getText().replaceAll(" ", "%").replaceAll("　", "%") + "%");

        //Throw the query and set the items if not empty
        long start = System.currentTimeMillis();
        List<Item> list = tquery.setHint(QueryHints.REFRESH, HintValues.TRUE).getResultList();
        long end = System.currentTimeMillis();
        System.out.println("Connection of \"" + Thread.currentThread().getStackTrace()[1].getMethodName() + "\" ; Elapsed Time : " + (end - start) + "ms");
        if(list.isEmpty()) {
            MessageBox.show(Translator.stage,"No record matched.","Information",MessageBox.OK);
            return;
        }
        searchResultTableFX.getItems().clear();
        searchResultTableFX.setItems(FXCollections.observableList(list));        
    }

    /**
     * Delete Item selected on <code>searchResultTableFX</code>.
     */
    @FXML
    private void deleteData() {
        //return if no data selected on the table
        if(searchResultTableFX.getSelectionModel().isEmpty()) {
            MessageBox.show(Translator.stage, "No data is selected.", "Error", MessageBox.OK);
            return;
        }
        
        //Final confirmation of the deletion of item
        Item item = searchResultTableFX.getSelectionModel().getSelectedItem();
        if(MessageBox.show(Translator.stage,
                "The item below will be deleted from Database.\n"
                        + "Are you sure you want to delete it?"
                        + "\nBarCode : " + item.getBarcode()
                        + "\nItemName : " + item.getItemname()
                        + "\nSize : " + item.getSize()
                        + "\nIngredients : " + item.getIngredients()
                        + "\nUses : " + item.getUses()
                        + "\nPrise : " + item.getCprice()
                , "Confirmation", MessageBox.NO|MessageBox.YES) != MessageBox.YES)
            return;
        
        //Delete item
        EntityManager em;
        try {
            em = ConnectItemPU.getInstance().getEM();
        } catch (IncorrectDatabaseFilePathException ex) {
            MessageBox.show(Translator.stage,
                    "Database file you specified seems to be no longer available.\n"
                            + "Please specify the available database file again.",
                    "Warning", MessageBox.OK);
            return;
        }
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            em.remove(item);
            tx.commit();
            em.clear();
            MessageBox.show(Translator.stage, "Successfully deleted the item.", "Information", MessageBox.OK);
            searchData();
        } catch (Exception ex) {
            Logger.getLogger(SearchTab.class.getName()).log(Level.SEVERE, null, ex);
            tx.rollback();
            em.clear();
            MessageBox.show(Translator.stage, "Failed to delete the item.", "Error", MessageBox.OK);
        }
    }
    
    /**
     * Show preview window of labels to be printed.
     */
    @FXML
    private void showPreviewWindow() {
        if(printedItemTableFX.getItems().isEmpty()) {
            MessageBox.show(Translator.stage, "No Item selected.", "Warning", MessageBox.OK);
            return;
        }

        ObservableList<Item> itemList = FXCollections.observableArrayList();

        //Add item into the list
        printedItemTableFX.getItems().stream().forEach((item) -> {
            //Each item should be printed "i" times
            for(int i = item.getPrintcounter(); i > 0 ; i--)
                itemList.add(item);
        });
        
        //Add specified number of blank(null) label into the list, to use label sheet effectively
        for(int i = skipLabelCounterFX.getValue(); i > 0; i--)
            itemList.add(0, null);
        
        //Preparing preview
        ObservableList<GridPane> sheetList = FXCollections.observableArrayList();

        //Load and setup sheet(s) to be printed
        while( ! itemList.isEmpty()) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PrintingLabel.fxml"));
            GridPane sheet;
            try {
                sheet = (GridPane) fxmlLoader.load();
            } catch (IOException ex) {
                Logger.getLogger(SearchTab.class.getName()).log(Level.SEVERE, null, ex);
                MessageBox.show(Translator.stage,
                        "Couldn't load the data into label template...\n"
                        + "Cannot process printing job.",
                        "Error", MessageBox.OK);
                return;
            }
            PrintingLabel printingLabel = fxmlLoader.getController();

            //determine the end of index this time, fill the data, and remove the data
            int end = itemList.size() >= 30 ? 30 : itemList.size();
            printingLabel.loadDataIntoLabels(itemList.subList(0, end));
            itemList.subList(0, end).clear();

            //add the sheet to the list of sheet
            sheetList.add(sheet);
        }
        
        //Load and setup preview window
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PreviewWindow.fxml"));
        VBox previewWindowVBox;
        try {
            previewWindowVBox = (VBox) fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(SearchTab.class.getName()).log(Level.SEVERE, null, ex);
            MessageBox.show(Translator.stage,
                    "Couldn't load the Preview template...\n"
                    + "Cannot process printing job.",
                    "Error", MessageBox.OK);
            return;
        }
        PreviewWindow previewWindow = fxmlLoader.getController();
        previewWindow.setup(sheetList);

        //show Preview Window
        Scene labelSheetScene = new Scene(previewWindowVBox);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(Translator.stage);
        stage.setScene(labelSheetScene);
        stage.setTitle("Preview");
        stage.show();
    }
    
    /**
     * Clear only selected to-be-printed label from <code>printedItemTableFX</code>.
     */
    @FXML
    private void clearSelectedLabelsPrinted() {
        List<Integer> selectedIndexList = printedItemTableFX.getSelectionModel().getSelectedIndices();
        //Nothing to do if selection is empty
        if(Objects.isNull(selectedIndexList) || selectedIndexList.isEmpty())
            return;
        ObservableList<Item> itemList = printedItemTableFX.getItems();
        //Nothing to do if table is empty
        if(Objects.isNull(itemList) || itemList.isEmpty())
            return;
        //If delete by Item, it work wrongly so delete by Index 
        selectedIndexList
                .stream()
                .sorted(Comparator.reverseOrder())
                .forEach( i -> itemList.remove(i.intValue()));
    }
    
    /**
     * Clear all the to-be-printed labels from <code>printedItemTableFX</code>.
     */
    @FXML
    private void clearAllLabelsPrinted() {
        ObservableList<Item> itemList = printedItemTableFX.getItems();
        //Nothing to do if table is empty
        if(Objects.isNull(itemList) || itemList.isEmpty())
            return;
        itemList.clear();
    }
}
