/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translator;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.control.Pagination;
import javafx.scene.layout.GridPane;
import jfx.messagebox.MessageBox;

/**
 * This is controller class for <code>PreviewWindow.fxml</code>.
 */
public class PreviewWindow implements Initializable {

    @FXML
    private Pagination paginationFX;

    @Override
    public void initialize(URL url, ResourceBundle rb) {}

    private ObservableList<GridPane> sheetList;
    /**
     * Load sheets in the list into pages in Pagination.
     * @param sheetList list of sheet to be loaded
     */
    public void setup(ObservableList<GridPane> sheetList) {
        this.sheetList = sheetList;
        paginationFX.setPageFactory(pageIndex -> sheetList.get(pageIndex));
        paginationFX.setPageCount(sheetList.size());
    }

    /**
     * Print the sheets.
     * <p><b>### Caution ###</b>
     * Although the paper size is specified in #createPageLayout method,
     * it seems not to work.
     */
    @FXML
    private void print() {
        PrinterJob job = PrinterJob.createPrinterJob();
        if (job == null)
            return;
        job.jobStatusProperty().addListener(status -> System.out.println("status : " + status));
        job.getJobSettings().setJobName("Label");
        Printer printer = Printer.getDefaultPrinter();
        if (Objects.isNull(printer)) {
            MessageBox.show(Translator.stage,
                    "No default printer is found.\n"
                    + "Cannot process printing job.", "Error", MessageBox.OK);
            return;
        }
        
        //left & right margin 13.68 = 0.19 * 72ppi top & bottom margin 36 =
        //0.5inch * 72ppi reference:
        //https://www.techwalla.com/articles/how-to-lay-out-avery-5160
        PageLayout layout = printer.createPageLayout(Paper.NA_LETTER, PageOrientation.PORTRAIT, 13.68, 13.68, 36, 36);
        if( ! job.showPrintDialog(Translator.stage))
            return;
        int i = 1;
        sheetList.forEach(sheet -> {
            try {
                if( ! job.printPage(layout, sheet))
                    MessageBox.show(Translator.stage,
                            "Error occured when printing a specific page.\n"
                                    + "Page No. : " + sheetList.indexOf(sheet),
                            "Error", MessageBox.OK);
            } catch (Exception ex) {
                Logger.getLogger(PreviewWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        ;
        if( ! job.endJob()) {
            MessageBox.show(Translator.stage, "Printing job has not been completed...", "Error", MessageBox.OK);
        }
    }
}
